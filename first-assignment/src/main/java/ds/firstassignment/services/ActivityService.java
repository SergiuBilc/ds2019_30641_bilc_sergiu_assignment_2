package ds.firstassignment.services;

import ds.firstassignment.entities.Activity;
import ds.firstassignment.repositories.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActivityService {

    @Autowired
    private ActivityRepository activityRepository;

    public void addActivity(Activity activity){
        activityRepository.save(activity);
    }
}
