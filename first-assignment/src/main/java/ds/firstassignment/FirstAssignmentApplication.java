package ds.firstassignment;

import ds.firstassignment.dto.PatientDTO;
import ds.firstassignment.dto.mapper.PatientMapper;
import ds.firstassignment.entities.Activity;
import ds.firstassignment.entities.Patient;
import ds.firstassignment.services.ActivityService;
import ds.firstassignment.services.PatientService;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.Date;

@SpringBootApplication
public class FirstAssignmentApplication {



    public static void main(String[] args) {
        SpringApplication.run(FirstAssignmentApplication.class, args);
    }


    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }


}
