package ds.firstassignment;

import ds.firstassignment.dto.PatientDTO;
import ds.firstassignment.dto.mapper.PatientMapper;
import ds.firstassignment.entities.Activity;
import ds.firstassignment.services.ActivityService;
import ds.firstassignment.services.PatientService;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@Configuration
public class MessageConsumer {

    @Autowired
    SimpMessagingTemplate websocket;

    @Autowired
    ActivityService activityService;

    @Autowired
    PatientMapper patientMapper;

    @Autowired
    PatientService patientService;
    @Bean
    public Queue myQueue() {
        return new Queue("activities", true, false, false, null);
    }

    @RabbitListener(queues = "activities")
    public void listen(CustomMessage alert) {
        PatientDTO patient = patientService.findById(Integer.parseInt(alert.getPatient_id()));
        Activity a = new Activity(alert.getActivity(), alert.getStartTime(), alert.getEndTime(), patientMapper.patientDTOtoPatient(patient));
        activityService.addActivity(a);

        alert.setPatient_id(patient.getName());
        if (doWork(alert)) {
            this.websocket.convertAndSend("/topic", alert);
        }

    }

    private Boolean doWork(CustomMessage activity) {

        long milliseconds = activity.getEndTime().getTime() - activity.getStartTime().getTime();
        int seconds = (int) milliseconds / 1000;

        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;

        switch (activity.getActivity()) {
            case "Sleeping":
                if (hours > 9) {
                    return true;
                }
                break;
            case "Toileting\t":
                if (minutes > 10) {
                    return true;
                }
                break;
            case "Leaving\t":
                if (hours > 3) {
                    return true;
                }
                break;
            default:
                return false;

        }
        return false;
    }
}
