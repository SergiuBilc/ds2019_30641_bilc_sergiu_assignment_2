package ds.firstassignment.dto;

import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.User;

import java.util.List;

public class DoctorDTO {
    private int id;
    private String name;
    private String address;
    private User user;


    public DoctorDTO() {
    }

    public DoctorDTO(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
