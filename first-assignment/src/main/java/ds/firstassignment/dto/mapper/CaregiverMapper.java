package ds.firstassignment.dto.mapper;

import ds.firstassignment.dto.CaregiverDTO;
import ds.firstassignment.dto.CaregiverWithPatientsDTO;
import ds.firstassignment.dto.DoctorDTO;
import ds.firstassignment.dto.DoctorViewDTO;
import ds.firstassignment.entities.Caregiver;
import ds.firstassignment.entities.Doctor;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CaregiverMapper {

    Caregiver fromCaregiverDTOtoCaregiverEntity(CaregiverDTO caregiverDTO);

    CaregiverDTO fromEntityToCaregiverDTO(Caregiver caregiver);

    List<Caregiver> fromCaregiverDTOListtoCaregiverEntityList(List<CaregiverDTO> caregiverDTO);

    List<CaregiverDTO> fromEntityListToCaregiverDTOList(List<Caregiver> caregiver);

    Caregiver fromCaregiverPatientDTOtoCaregiverEntity(CaregiverWithPatientsDTO caregiverDTO);

    CaregiverWithPatientsDTO fromEntityToCaregiverPatientDTO(Caregiver caregiver);

}

