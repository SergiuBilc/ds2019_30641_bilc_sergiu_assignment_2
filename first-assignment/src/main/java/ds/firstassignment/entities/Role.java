package ds.firstassignment.entities;

public enum Role {
    DOCTOR,
    CAREGIVER,
    PATIENT
}
