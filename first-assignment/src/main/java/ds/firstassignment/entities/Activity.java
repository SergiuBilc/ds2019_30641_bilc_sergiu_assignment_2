package ds.firstassignment.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name="Activity")
public class Activity {


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "name", length = 100)
    private String activity;

    @Column(name = "start", length = 100)
    private Timestamp start;

    @Column(name = "end", length = 100)
    private Timestamp end;

    @ManyToOne
    @JoinColumn(name = "patient_id")
    private Patient patient;

    public Activity() {
    }

    public Activity(String activity, Timestamp start, Timestamp end, Patient patient) {
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.patient = patient;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id=" + id +
                ", activity='" + activity + '\'' +
                ", startTime=" + start +
                ", endTime=" + end +
                ", patient=" + patient +
                '}';
    }
}
