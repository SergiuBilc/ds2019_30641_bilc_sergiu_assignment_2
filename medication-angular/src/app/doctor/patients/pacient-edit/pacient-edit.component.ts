import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Pacient } from 'src/app/_models/pacient';
import { PatientService } from 'src/app/_services/patient.service';
import { User } from 'src/app/_models/user';
import { Role } from 'src/app/_models/role';
import { CaregiverService } from 'src/app/_services/caregiver.service';
import { Caregiver } from 'src/app/_models/caregiver';



@Component({
  selector: 'app-pacient-edit',
  templateUrl: './pacient-edit.component.html',
  styleUrls: ['./pacient-edit.component.css']
})
export class PacientEditComponent implements OnInit {

  username: string = '';
  password: string = '';
  patient: Pacient;
  caregivers: Caregiver[];
  selectedCaregiver: Caregiver;
  user: User;
  myGroup: FormGroup;

  isFormReady: boolean = false;


  @Output() addEmitter = new EventEmitter<any>();

  editPatient: boolean = true;

  constructor(public bsModalRef: BsModalRef,
    private patientService: PatientService,
    private caregiverService: CaregiverService,
    private formBuilder: FormBuilder) {
  }

  ngOnInit() {

    this.myGroup = this.initForm();

    this.caregiverService.getAll().subscribe(
      (data) => {
        this.caregivers = data;
        if (this.editPatient) {
          let caregiverId = this.caregivers.findIndex(value => value.id == this.patient.caregiver.id);
          this.myGroup.controls.patientData.patchValue({caregivers: this.caregivers[caregiverId].id});
        } else {
          this.myGroup.controls.patientData.patchValue({caregivers: this.caregivers[0].id});
        }
      }
    );
    this.isFormReady = true;

    if (this.editPatient) {
      this.patientService.getUser(this.patient).subscribe(
        (response) => {
          this.user = response;
          this.username = this.user.username;
          this.password = this.user.password;
          this.updateForm();
        }
      );

      this.isFormReady = true;
    }


  }

  private updateForm() {
    this.myGroup.controls.userData.patchValue({ username: this.username, password: this.password });
    this.myGroup.controls.patientData.patchValue(
      {
        gender: this.patient.gender,
        medicalRecord: this.patient.medicalRecord,
        name: this.patient.name,
        address: this.patient.address,
        birthDate: this.patient.birthDate,
        // caregivers: this.caregivers
      }
    );
  }

  private initForm() {
    return new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null),
        'password': new FormControl(null),
      }),
      'patientData': new FormGroup({
        'gender': new FormControl(null),
        'medicalRecord': new FormControl(null),
        'name': new FormControl(null),
        'address': new FormControl(null),
        'birthDate': new FormControl(null),
        'caregivers': new FormControl([]),
      })
    });
  }


  getCaregivers() {

  }

  handleSubmit() {
    if (this.editPatient) {
      let patient: Pacient = this.computePatient();
      let user: User = this.computeUser();

      console.log(patient);
      this.patientService.updatePatient(user, patient, this.myGroup.value.caregivers).subscribe(
        res => {
          console.log(res);
          this.addEmitter.emit();
          this.bsModalRef.hide();
        }
      );
    } else {
      let user: User = this.computeUser();
      let patient: Pacient = this.computePatient();
      this.patientService.addPatient(user, patient, this.myGroup.value.caregivers).subscribe(
        res => {
          console.log(res);
          this.addEmitter.emit();
          this.bsModalRef.hide();
        }
      );
    }
  }


  private computeUser(): User {
    if (this.editPatient) {
      return new User(this.user.id, this.myGroup.value.username, this.myGroup.value.password, Role.Patient);

    } else {
      return new User(null, this.myGroup.value.username, this.myGroup.value.password, Role.Patient);
    }
  }

  private computePatient(): Pacient {
    console.log(this.patient.id);
    if (this.editPatient) {
      return new Pacient(this.patient.id, this.myGroup.value.name, this.myGroup.value.birthDate, this.myGroup.value.gender, this.myGroup.value.address, this.myGroup.value.medicalRecord);
    } else {
      let patient: Pacient = new Pacient(null, this.myGroup.value.name, this.myGroup.value.birthDate, this.myGroup.value.gender, this.myGroup.value.address, this.myGroup.value.medicalRecord);
      patient.id = null;
      return patient;
    }
  }
}
