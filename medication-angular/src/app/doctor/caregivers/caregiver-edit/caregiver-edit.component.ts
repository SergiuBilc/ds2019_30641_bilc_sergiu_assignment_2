import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Caregiver } from 'src/app/_models/caregiver';
import { Doctor } from 'src/app/_models/doctor';
import { FormGroup, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { CaregiverService } from 'src/app/_services/caregiver.service';
import { DoctorService } from 'src/app/_services/doctor.service';
import { User } from 'src/app/_models/user';
import { Role } from 'src/app/_models/role';

@Component({
  selector: 'app-caregiver-edit',
  templateUrl: './caregiver-edit.component.html',
  styleUrls: ['./caregiver-edit.component.css']
})
export class CaregiverEditComponent implements OnInit, OnDestroy {

  username: string = '';
  password: string = '';
  caregiver: Caregiver;
  doctors: Doctor[];
  user: User;
  isFormReady: boolean = false;
  selectedDoctorId: number;

  @Output() addEmitter = new EventEmitter<any>();

  myGroup: FormGroup;

  editCaregiver: boolean = true;

  constructor(public bsModalRef: BsModalRef,
    private caregiverService: CaregiverService,
    private doctorService: DoctorService) {

  }

  ngOnInit() {
    this.myGroup = this.initForm();
    console.log(this.myGroup.controls);
    this.fetchDoctorsAndPrepareForm();

    if (this.editCaregiver) {
      this.fetchUserAndPrepareForm();
    }
  }

  private fetchDoctorsAndPrepareForm() {
    this.doctorService.getAll().subscribe((data) => {
      this.doctors = data;
      this.isFormReady = true;
      if (this.editCaregiver) {
        this.selectedDoctorId = this.doctors.findIndex(value => value.id == this.caregiver.doctor.id);
        this.myGroup.controls.caregiverData.patchValue({doctors: this.doctors[this.selectedDoctorId].id});
      }
      else {
        this.myGroup.controls.caregiverData.patchValue({doctors: this.doctors[0].id});
      }
    });
  }

  private fetchUserAndPrepareForm() {
    this.caregiverService.getUser(this.caregiver).subscribe((response) => {
      this.user = response;
      this.username = this.user.username;
      this.password = this.user.password;
      this.updateFormForUpdateMode();
      this.isFormReady = true;
    });
  }

  private initForm() {
    return new FormGroup({
      'userData': new FormGroup({
        'username': new FormControl(null),
        'password': new FormControl(null),
      }),
      'caregiverData': new FormGroup({
        'gender': new FormControl('M'),
        'name': new FormControl(null),
        'address': new FormControl(null),
        'birthDate': new FormControl(null),
        'doctors': new FormControl(['']),
      })
    });
  }

  private updateFormForUpdateMode() {
    this.myGroup.controls.userData.patchValue({ username: this.username, password: this.password });
    this.myGroup.controls.caregiverData.patchValue(
      {
        gender: this.caregiver.gender,
        name: this.caregiver.name,
        address: this.caregiver.address,
        birthDate: this.caregiver.birthDate
      }
    );
  }

  ngOnDestroy() {
    this.isFormReady = false;
  }

  handleSubmit() {
    if (this.editCaregiver) {
      this.updateCaregiver();
    } else {
      this.addCaregiver();
    }
  }

  private addCaregiver() {
    let user: User = this.computeUser();
    let caregiver: Caregiver = this.computeCaregiver();
    this.caregiverService.addCaregiver(user, caregiver, this.myGroup.value.doctors).subscribe(res => {

      this.addEmitter.emit();
      this.bsModalRef.hide();
    });
  }

  private updateCaregiver() {
    let caregiver: Caregiver = this.computeCaregiver();
    let user: User = this.computeUser();
    this.caregiverService.updateCaregiver(user, caregiver, this.myGroup.value.doctors).subscribe(res => {

      this.addEmitter.emit();
      this.bsModalRef.hide();
    });
  }

  private computeUser(): User {
    if (this.editCaregiver) {
      return new User(this.user.id, this.myGroup.value.username, this.myGroup.value.password, Role.Caregiver);
    } else {
      return new User(null, this.myGroup.value.username, this.myGroup.value.password, Role.Caregiver);
    }
  }

  private computeCaregiver(): Caregiver {
    if (this.editCaregiver) {
      return new Caregiver(this.caregiver.id, this.myGroup.value.name, this.myGroup.value.birthDate, this.myGroup.value.gender, this.myGroup.value.address);
    } else {
      return new Caregiver(null, this.myGroup.value.name, this.myGroup.value.birthDate, this.myGroup.value.gender, this.myGroup.value.address);

    }
  }



}
