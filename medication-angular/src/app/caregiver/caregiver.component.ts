import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Caregiver } from '../_models/caregiver';
import { CaregiverService } from '../_services/caregiver.service';
import { User } from '../_models/user';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  private readyToRender: boolean = false;
  private currentCaregiverSubject: BehaviorSubject<Caregiver>;
  public caregiver: Observable<Caregiver>;

  getCaregiverEmitter: Observable<Caregiver>;

  constructor(private caregiverService: CaregiverService, ) {
    this.currentCaregiverSubject = new BehaviorSubject<Caregiver>(new Caregiver(null,null,null,null,null));
    this.caregiver = this.currentCaregiverSubject.asObservable();
  }
  ngOnInit() {
    this.computeDoctor();
  }

  public get currentUserValue(): Caregiver {
    return this.currentCaregiverSubject.value;
  }

  computeDoctor() {
    let user: User = JSON.parse(localStorage.getItem('currentUser'));
    console.log("Current doctor: " + user);
    let doctor: Caregiver;
  this.caregiverService.getMyProfile(user).subscribe(res => {
      this.currentCaregiverSubject.next(res);
      this.readyToRender = true;
    });



  }

}
