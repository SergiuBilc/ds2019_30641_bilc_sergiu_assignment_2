import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Medication } from '../_models/medication';


@Injectable({ providedIn: 'root' })
export class MedicationService {
    constructor(private http: HttpClient) { }

    getAll(): Observable<Medication[]> {
        return this.http.get<Medication[]>(`medication/all`);
    }

    getById(id: number) {
        return this.http.get<Medication>(`medication/${id}`);
    }

    updateMedication( medication: Medication) {
        return this.http.put<Medication>('medication/update', medication);
    }

    addMedication( medication: Medication) {
        
        return this.http.post<any>('medication/add', medication );
    }

    deleteMedication(id: number) {
        return this.http.delete<number>('medication/delete', {params: {medicationId: id+''}});
    }


}