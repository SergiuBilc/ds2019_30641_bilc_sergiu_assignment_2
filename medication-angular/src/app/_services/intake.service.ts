import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Medication } from '../_models/medication';
import { Intake } from '../_models/intake';


@Injectable({ providedIn: 'root' })
export class Intakeservice {
    constructor(private http: HttpClient) { }

    getAll(): Observable<Intake[]> {
        return this.http.get<Intake[]>(`intake/all`);
    }

    getById(id: number) {
        return this.http.get<Intake>(`intake/${id}`);
    }

    getMyIntakes(id: number){
        return this.http.get<Intake[]>(`patient/myIntakes`, { params: { idPatient: id+''}});
    }


    updateMedication( medication: Intake) {
        return this.http.put<Intake>('intake/update', medication);
    }

    addMedication( medication: Intake) {
        
        return this.http.post<any>('intake/add', medication );
    }

    deleteMedication(id: Intake) {
        return this.http.delete<number>('intake/delete', {params: {medicationId: id+''}});
    }


}