import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../_models/user';
import { Pacient } from '../_models/pacient';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class PatientService {
 
    constructor(private http: HttpClient) { }

    getMyProfile(user: User){
        return this.http.post<Pacient>(`patient/profile`, user);
    }

    getAll(): Observable<Pacient[]> {
        return this.http.get<Pacient[]>(`patient/all`);
    }

    getById(id: number) {
        return this.http.get<Pacient>(`patient/${id}`);
    }

    updatePatient(user: User, patient: Pacient, idCaregiver: number) {
        let newDTO: { userDTO: User, patientDTO: Pacient } = { userDTO: user, patientDTO: patient };
        console.log(patient);
        return this.http.put<Pacient>('patient/update', newDTO,  { params: { caregiverId: idCaregiver+''} });
    }

    addPatient(user: User, patient: Pacient, idCaregiver: number) {
        let newDTO: { userDTO: User, patientDTO: Pacient } = { userDTO: user, patientDTO: patient };
        console.log(newDTO);
        return this.http.post<any>('patient/add', newDTO, { params: { caregiverId: idCaregiver+''} });
    }

    deletePatient(id: number) {
        return this.http.delete<number>('patient/delete', {params: {patientId: id+''}});
    }

    getUser(patient: Pacient){
        return this.http.post<User>('patient/getUser', patient);
    }

}