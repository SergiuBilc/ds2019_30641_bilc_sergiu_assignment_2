import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { User } from './_models/user';
import { Router } from '@angular/router';
import { AuthenticationService } from './_services/authentication.service';
import { Role } from './_models/role';
import { WebSocketAPIClass } from './WebSocketAPI';

import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';
import { PatientAlert } from './_models/patientAlert';
import { AlertComponentComponent } from './shared/alert-component/alert-component.component';
import { PlaceholderDirective } from './shared/placeholder/placeholder.directive';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  currentUser: User;
  greeting: any;
  name: string;
  websocket: Stomp;
  websocketAPI: WebSocketAPIClass = new WebSocketAPIClass(this);

  @ViewChild(PlaceholderDirective, { static: false }) alertHost: PlaceholderDirective;


  constructor(private router: Router,
    private authenticationService: AuthenticationService,
    private toastrService: ToastrService
  ) {
    this.authenticationService.currentUser.subscribe(x => {
      this.currentUser = x;

      if (this.currentUser && this.currentUser.role == Role.Caregiver) {
        this.websocketAPI._connect();
      }
    });

  }

  ngOnInit() {
    this.websocket = new WebSocketAPIClass(this);
  }


  get isDoctor() {
    return this.currentUser && this.currentUser.role === Role.Doctor;
  }


  get isCaregiver() {
    return this.currentUser && this.currentUser.role === Role.Caregiver;
  }


  get isPatient() {
    return this.currentUser && this.currentUser.role === Role.Patient;
  }

  logout() {
    if (this.currentUser && this.currentUser.role == Role.Caregiver) {
      // console.log(this.websocketA);
      this.websocketAPI._disconnect();
     
    }
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  showPatientAlert(patientAlert: PatientAlert) {

    let message = "";
    if (patientAlert.activity === "Sleeping") {
      message = "Patient " + patientAlert.patient_id + " is asleep for than 12 hours!";
    } else {
      if (patientAlert.activity.startsWith("Toileting")) {
        message = "Patient " + patientAlert.patient_id + " spent too much time toileting!";
      } else {
        if (patientAlert.activity.startsWith("Leaving")) {
          message = "Patient " + patientAlert.patient_id + " didnt come home soon enough!";
        }
      }
    }
     this.toastrService.error(message, "Alert", {
      disableTimeOut: true,
      positionClass: 'toast-bottom-right',
      closeButton: true
    });


  }

}
