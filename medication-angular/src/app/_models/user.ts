import { Role } from './role';

export class User {
    id: number;
    username: string;
    password: string;
    name: string;
    role: Role;
    token?: string;

    constructor(id: number, username1: string, password1: string, role1: Role) {
        this.username = username1;
        this.password = password1;
        this.role = role1;
        this.id = id;
    }
}