
export interface PatientAlert{
    patient_id: string;
    activity: string;
    startTime: Date;
    endTime: Date;
}