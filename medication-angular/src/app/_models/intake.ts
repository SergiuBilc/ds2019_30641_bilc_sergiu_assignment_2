import { Medication } from './medication';

export class Intake{
    id: number;
    startDate: Date;
    endDate: Date;
    datails: string;
    medication: Medication
}