import { User } from './user';
import { Caregiver } from './caregiver';

export class Doctor{
    id: number;
    name: string;
    address: string;
    user: User;
    caregivers: Caregiver[];
}