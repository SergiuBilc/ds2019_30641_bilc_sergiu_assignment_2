import { Intake } from './intake';
import { Caregiver } from './caregiver';

export class Pacient {
    id: number;
    name: string;
    birthDate: Date;
    gender: string;
    address: string;
    medicalRecord: string;
    caregiver: Caregiver;
    medicationPlans?: Intake[];

    constructor( id: number,
        name: string,
        birthDate: Date,
        gender: string,
        address: string,
        medicalRecord: string,){

            this.id = id;
            this.name = name;
            this.birthDate = birthDate;
            this.gender = gender;
            this.address = address;
            this.medicalRecord = medicalRecord
    }
}