export enum Role{
    Doctor = 'doctor',
    Caregiver = 'caregiver',
    Patient = 'patient'
}