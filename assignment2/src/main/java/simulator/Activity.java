package simulator;

import java.util.Date;

public class Activity {

    private Integer patient_id;
    private String activity;
    private Date startTime;
    private Date endTime;


    public Activity(String activity, Date startTime, Date endTime) {
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Activity(Integer patient_id, String activity, Date startTime, Date endTime) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(Integer patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "activity='" + activity + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                '}';
    }
}
