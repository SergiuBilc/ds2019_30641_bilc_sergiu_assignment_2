package simulator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class Producer {


    private final static String QUEUE_NAME = "activities";
    Connection connection;
    Channel channel;
    ObjectMapper mapper = new ObjectMapper();

    public Producer() {
        setupQueue();
    }

    public void processListOfActivities(List<Activity> activities) {
        try {
            for (int i = 0; i < activities.size(); i++) {
                if (i % 10 == 0) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        System.out.print("error");
                    }
                }
                activities.get(i).setPatient_id(1);
                sendMessage(activities.get(i));
            }
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }

    private void setupQueue() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try {
            this.connection = factory.newConnection();
            this.channel = connection.createChannel();
            this.channel.queueDeclare(QUEUE_NAME, true, false, false, null);
            // for (Activity activity : activities) {

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (
                IOException e) {
            e.printStackTrace();
        }
    }

    private void sendMessage(Activity activity) throws IOException {
        String message = mapper.writeValueAsString(activity);
        AMQP.BasicProperties messageProperties = new AMQP.BasicProperties.Builder()
                .timestamp(new Date())
                .contentType("application/json")
                .priority(1)
                .build();

        channel.basicPublish("", QUEUE_NAME, messageProperties, message.getBytes());
        System.out.println(" [x] Sent '" + message + "'");
    }


}
