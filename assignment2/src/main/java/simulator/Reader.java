package simulator;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Reader {

    public List<Activity> readFromFile(String filename) {
        List<Activity> activities = new ArrayList<Activity>();
        File file = new File(filename);

        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String st;
            while ((st = br.readLine()) != null) {
                activities.add(parseLineToActivity(st));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return activities;
    }

    private Activity parseLineToActivity(String st) {
        String[] values = st.split("\t\t");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            return new Activity(values[2], format.parse(values[0]), format.parse(values[1]));
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
