import simulator.Activity;
import simulator.Producer;
import simulator.Reader;
import java.util.List;

public class
App {

    public static void main(String[] args) {
        Reader reader = new Reader();
        List<Activity> activities = reader.readFromFile("activity.txt");
        System.out.println(activities.get(0));

        Producer producer = new Producer();
        producer.processListOfActivities(activities);
    }
}
